# Foires aux questions

La [FAQ DCIR 09 2019](../../files/Cnam/2019-07-CNAM-FAQ_DCIR_MLP-2.0.xlsx) et la [FAQ EGB 09 2019](../../files/Cnam/2019-06-CNAM-FAQ_EGB_MLP-2.0.xlsx) [CNAM - MPL-2.0] reprennent les questions fréquemment posées sur le DCIR et sur l'EGB.
